PROJECT             = fibonacci
BUILD_DIR           ?= build

# Version information
VERSION_MAJOR       = 1
VERSION_MINOR       = 2
VERSION_BUGFIX      = 3
VERSION_GIT         := $(shell git describe --tag --always --abbrev=5 --dirty)

# Application sources and artifacts
APP_BIN             = $(BUILD_DIR)/$(PROJECT)
APP_SOURCES         = src/IFibonacciNumbers.cpp \
                      src/FibonacciNumbersRecursed.cpp \
                      src/FibonacciNumbersDynamic.cpp
APP_MAIN            = src/main.cpp
APP_OBJS            = $(patsubst %.cpp,$(BUILD_DIR)/%.o,$(APP_SOURCES) $(APP_MAIN))

# Test sources and artifacts
TEST_BIN            = $(BUILD_DIR)/$(PROJECT)_tests
TEST_SOURCES        = $(APP_SOURCES) \
                      tests/FibonacciNumbersRecursedTest.cpp \
                      tests/FibonacciNumbersDynamicTest.cpp \
                      tests/main.cpp
TEST_OBJS           = $(patsubst %.cpp,$(BUILD_DIR)/tests/%.o,$(TEST_SOURCES))

# Generated depedecy files
DEPS                = $(APP_OBJS:.o=.d) \
                      $(TEST_OBJS:.o=.d)

# Compiler options
COMMON_CFLAGS       = -Wall -Werror -Wextra -MMD

# Debug/Release mode
ifneq ($(DEBUG),)
  COMMON_CFLAGS     += -g
  BUILD_DIR         := $(BUILD_DIR)/debug
else
  COMMON_CFLAGS     += -DNDEBUG -O3
  BUILD_DIR         := $(BUILD_DIR)/release
endif

# CppUTest flags
ifneq ($(CPPUTEST_HOME),)
  HAS_CPPUTEST          = 1
  CPPUTEST_FLAGS        = -I$(CPPUTEST_HOME)/include
  CPPUTEST_LDFLAGS      = -L$(CPPUTEST_HOME)/lib -lCppUTest -lCppUTestExt
else
  HAS_CPPUTEST          = $(shell pkg-config cpputest && echo 1)
  ifeq ($(HAS_CPPUTEST),1)
    CPPUTEST_FLAGS      = $(shell pkg-config --cflags cpputest 2>/dev/null)
    CPPUTEST_LDFLAGS    = $(shell pkg-config --libs cpputest 2>/dev/null)
  endif
endif

CFLAGS              += $(COMMON_CFLAGS)
CXXFLAGS            += $(COMMON_CFLAGS) -std=c++14

# Silence make
ifneq ($(V),)
  SILENCE           =
else
  SILENCE           = @
endif

# Fancy output
SHOW_COMMAND        := @printf "%-15s%s\n"
SHOW_CXX            := $(SHOW_COMMAND) "[ $(CXX) ]"
SHOW_CLEAN          := $(SHOW_COMMAND) "[ CLEAN ]"
SHOW_GEN            := $(SHOW_COMMAND) "[ GEN ]"

##############################################################################################
# Default target and help message
##############################################################################################
DEFAULT_TARGET =  $(APP_BIN)

ifeq ($(HAS_CPPUTEST),1)
  DEFAULT_TARGET += run_tests
endif

all: $(DEFAULT_TARGET)
.PHONY: all

# Take care of compiler generated depedencies
-include $(DEPS)

##############################################################################################
# Version generated file
##############################################################################################
src/Version.hpp: src/Version.hpp.in Makefile $(APP_SOURCES)
	$(SHOW_GEN) $@
	$(SILENCE)sed -e's/%%VERSION_MAJOR%%/$(VERSION_MAJOR)/g' \
	  -e 's/%%VERSION_MINOR%%/$(VERSION_MINOR)/g' \
	  -e 's/%%VERSION_BUGFIX%%/$(VERSION_BUGFIX)/g' \
	  -e 's/%%VERSION_GIT%%/$(VERSION_GIT)/g' \
	  $< > $@

##############################################################################################
# Application
##############################################################################################
$(APP_BIN): src/Version.hpp $(APP_OBJS)
	$(SHOW_CXX) $@
	$(SILENCE)$(CXX) -o $@ $(APP_OBJS)

$(BUILD_DIR)/%.o: %.cpp
	$(SHOW_CXX) $@
	$(SILENCE)mkdir -p $(dir $@)
	$(SILENCE)$(CXX) $(CXXFLAGS) -c $< -o $@

##############################################################################################
# Tests
##############################################################################################
tests: $(TEST_BIN)
.PHONY: tests

run_tests: $(BUILD_DIR)/.tests_passed
.PHONY: run_tests

$(BUILD_DIR)/.tests_passed: $(TEST_BIN)
	$(SILENCE)./$< || rm $<
	$(SILENCE)touch $@

 $(TEST_BIN): $(TEST_OBJS)
	$(SHOW_CXX) $@
	$(SILENCE)$(CXX) $(TEST_OBJS) $(CPPUTEST_LDFLAGS) -o $@

$(BUILD_DIR)/tests/%.o: %.cpp
ifneq ($(HAS_CPPUTEST),1)
	$(error CppUTest not found, cannot build the tests)
endif
	$(SHOW_CXX) $@
	$(SILENCE)mkdir -p $(dir $@)
	$(SILENCE)$(CXX) $(CXXFLAGS) $(CPPUTEST_FLAGS) -c $< -o $@

##############################################################################################
# Cleanup
##############################################################################################
clean:
	$(SHOW_CLEAN) $(BUILD_DIR)
	$(SILENCE)rm -rf $(BUILD_DIR) src/Version.hpp
.PHONY: clean