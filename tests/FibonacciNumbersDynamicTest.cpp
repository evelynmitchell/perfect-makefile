/**
 *         DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                    Version 2, December 2004
 *
 * Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */
#include <CppUTest/TestHarness.h>

#include "../src/FibonacciNumbersDynamic.hpp"

//------------------------------------------------------------------------------
TEST_GROUP(FibonacciDynamic)
{
  TEST_SETUP()
  {
  }

  TEST_TEARDOWN()
  {
  }
};

//------------------------------------------------------------------------------
TEST(FibonacciDynamic, Fibo3Is2)
{
  fibo::FibonacciNumbersDynamic fibo;

  CHECK_EQUAL(2, fibo.getNumber(3));
}

//------------------------------------------------------------------------------
TEST(FibonacciDynamic, Fibo2Is1)
{
  fibo::FibonacciNumbersDynamic fibo;

  CHECK_EQUAL(1, fibo.getNumber(2));
}

//------------------------------------------------------------------------------
TEST(FibonacciDynamic, Fibo1Is1)
{
  fibo::FibonacciNumbersDynamic fibo;

  CHECK_EQUAL(1, fibo.getNumber(1));
}

//------------------------------------------------------------------------------
TEST(FibonacciDynamic, Fibo0Is0)
{
  fibo::FibonacciNumbersDynamic fibo;

  CHECK_EQUAL(0, fibo.getNumber(0));
}
