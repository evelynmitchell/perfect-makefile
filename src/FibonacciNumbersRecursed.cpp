/**
 *         DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                    Version 2, December 2004
 *
 * Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */
#include "FibonacciNumbersRecursed.hpp"

namespace fibo {

//------------------------------------------------------------------------------
FibonacciNumbersRecursed::FibonacciNumbersRecursed()
{
}

//------------------------------------------------------------------------------
FibonacciNumbersRecursed::~FibonacciNumbersRecursed()
{
}

//------------------------------------------------------------------------------
std::string FibonacciNumbersRecursed::getName()
{
  return "recursive";
}

//------------------------------------------------------------------------------
unsigned int FibonacciNumbersRecursed::getNumber(unsigned int n)
{
  if(n == 0 || n == 1)
    return n;

  return getNumber(n - 1) + getNumber(n - 2);
};

}
