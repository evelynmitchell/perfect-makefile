/**
 *         DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                    Version 2, December 2004
 *
 * Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */
#ifndef _FIBO_FIBONACCI_NUMBERS_RECURSED_HPP_
#define _FIBO_FIBONACCI_NUMBERS_RECURSED_HPP_

#include "IFibonacciNumbers.hpp"

namespace fibo {

class FibonacciNumbersRecursed:
    public IFibonacciNumbers
{
public:
  FibonacciNumbersRecursed();
  virtual ~FibonacciNumbersRecursed();

  virtual std::string getName() override;
  virtual unsigned int getNumber(unsigned int n) override;
};

}
#endif  // _FIBO_FIBONACCI_NUMBERS_RECURSED_HPP_
