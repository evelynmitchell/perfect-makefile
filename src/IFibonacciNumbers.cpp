/**
 *         DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                    Version 2, December 2004
 *
 * Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */
#include "FibonacciNumbersRecursed.hpp"
#include "FibonacciNumbersDynamic.hpp"

namespace fibo {

//------------------------------------------------------------------------------
IFibonacciNumbers::IFibonacciNumbers()
{
}

//------------------------------------------------------------------------------
IFibonacciNumbers::~IFibonacciNumbers()
{
}

//------------------------------------------------------------------------------
int IFibonacciNumbers::create(
    std::unique_ptr<IFibonacciNumbers>& fibonacciNumber, Type type)
{
  switch(type)
  {
  case RECURSIVE:
    fibonacciNumber.reset(new FibonacciNumbersRecursed());
    break;
  case DYNAMIC:
    fibonacciNumber.reset(new FibonacciNumbersDynamic());
    break;
  default:
    return -1;
  }

  return 0;
}

}
